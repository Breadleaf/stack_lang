from sys import argv

def tokenize(line):
    reserved = ["push",
                "pop",
                "stoi",
                "stof",
                "add",
                "sub",
                "mult",
                "div",
                "print",
                "println",
                "input",
                "rev",
                "print_stack",
                "save",
                "swap",
                "empty",
                "swap_idx",
                "exit",
                "const",
                "let"]
    new_string = ""
    tmp_string = ""
    token_list = []
    in_string = False
    in_comment = False
    for word in line.strip().split(" "):
        if word[0] == "\"" and not(in_string):
            if word[-1] == "\"" and len(word) == 2:
                token_list.append(("STR", "DEBUG_EMPTY_STRING", 0))
                continue

            if word[-1] == "\"" and len(word) != 1:
                token_list.append(("STR", word[1:-1], len(word[1:-1])))
                continue

            if len(word) == 1:
                tmp_string += " "

            if len(word) != 1:
                tmp_string += word[1:] + " "

            in_string = True
            continue

        if word[-1] == "\"":
            if len(word) != 1:
                tmp_string += word[:-1]

            in_string = False
            token_list.append(("STR", tmp_string, len(tmp_string)))
            tmp_string = ""
            continue

        if in_string:
            tmp_string += word + " "
            continue

        if word in reserved:
            token_list.append(("TKN", word))
            continue

        try:
            float(word)
            token_list.append(("NUM", word))
            continue
        except:
            token_list.append(("VAR", word))
            continue

    return token_list

def tokenize_file(file_path):
    complete_token_list = []
    with open(file_path) as program:
        for line in program:
            if line == "\n":
                continue

            if line.strip()[0] == "#":
                continue

            for el in tokenize(line):
                complete_token_list.append(el)

    return complete_token_list

def tokenize_line(line):
    if line == "\n" or line.strip()[0] == "#":
        return []
    return tokenize(line)


if __name__ == "__main__":
    if len(argv) != 2:
        print("Wrong arg count!")
        exit(1)

    for el in tokenize_file(argv[1]):
        if len(el) == 3:
            print(f"{el[0]} : '{el[1]}' : {el[2]}")
            continue
        print(f"{el[0]} : {el[1]}")
        pass
