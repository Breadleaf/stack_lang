cmds = [["for", 2], ["for", 3], ["print"], ["end"], ["end"]]

def is_key(word):
    return word in ["for", "print", "end"]
