from sys import argv

stack = []
debug = True
max_memory = 71

def checkMemory(msg):
    current_memory_use = stack.__sizeof__()
    if debug: print(f"[DEBUG] {msg}")
    if debug: print(f"[DEBUG] {current_memory_use} / {max_memory}")
    if debug: print(f"[DEBUG] {stack}")
    if current_memory_use > max_memory:
        print("Error: exceeded total memory allocation")
        exit(1)

def virtualize(cmd):
    checkMemory("Pre-check")
    cmd()
    if debug: print()
    checkMemory("After-check")
    if debug: print()

virtualize(lambda: stack.append("!\n"))
virtualize(lambda: stack.append("enter your name: "))
virtualize(lambda: stack.append(input(stack.pop())))
virtualize(lambda: stack.append("Hello "))
virtualize(lambda: print(stack.pop(), end=""))
virtualize(lambda: print(stack.pop(), end=""))
virtualize(lambda: print(stack.pop(), end=""))
