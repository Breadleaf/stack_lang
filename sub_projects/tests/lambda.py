def lambda_maker(params, function):
    return eval(f"lambda {params} : {function}")

def if_maker(comparison, commands):
    return f"if {comparison}: {commands}"

