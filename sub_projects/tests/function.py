def list_to_str(_list):
    string = ""
    for idx in range(len(_list) - 1):
        string += _list[idx]
        string += ", "
    string += _list[-1]
    return string

def func_maker(name, params_list, cmds):
    code = f'''def {name}({list_to_str(params_list)}):
    {cmds}'''
    return code

exec(func_maker("add", ["a", "b"], "return a+b"))
print(add(10, 20))
