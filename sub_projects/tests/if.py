def if_maker(clause, cmds):
    code = f'''if ({clause}):
        {cmds}
    '''
    return code

exec(if_maker("1 == 1", "print(\"True\")"))
