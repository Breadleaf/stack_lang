def statement_combiner(_list):
    string = ""
    for idx in range(len(_list) - 1):
        string += _list[idx]
        string += "\n"
    string += _list[-1]
    return string

def list_to_str(_list):
    string = ""
    for idx in range(len(_list) - 1):
        string += _list[idx]
        string += ", "
    string += _list[-1]
    return string

#def func_maker(name, params_list, cmds):
#    code = f'''def {name}({list_to_str(params_list) if (len(params_list) > 1) else params_list[0]}):
#    {cmds}'''
#    return code

def func_maker(name, params_list, cmds):
    code = f'''def {name}({params_list}):
    {cmds}'''
    return code

def statement_maker(statement, clause, cmds):
    code = f'''{statement} ({clause}):
        {cmds}'''
    return code

def else_maker(cmds):
    code = f'''else:
        {cmds}'''
    return code
