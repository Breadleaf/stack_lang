import subprocess

def run_command(cmd):
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)
    return result.stdout.decode('utf-8')

def main():
    ps1 = ">>> "
    history = ""
    while True:
        try:
            current_cmd = str(input(ps1))
            if current_cmd == "py_exit":
                print("Goodbye!")
                exit(0)
            if current_cmd == "py_last":
                print(run_command(history), end="")
                continue
            print(run_command(current_cmd), end="")
            history = current_cmd
        except KeyboardInterrupt:
            print("\nGoodbye!")
            exit(0)
        except Exception as ex:
            print(f"Error: {ex}")

if __name__ == "__main__":
    main()
