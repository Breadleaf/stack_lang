#!/bin/python3

from sys import argv as _argv

main = []

tokens = [
        "PUSH",
        "POP",
        "ADD",
        "SUB",
        "PRINT",
        "PRINTLN",
]

def cmd_push(x):
    return (tokens[0], x)

def cmd_pop():
    return (tokens[1], )

def cmd_add():
    return (tokens[2], )

def cmd_sub():
    return (tokens[3], )

def cmd_print():
    return (tokens[4], )

def cmd_println():
    return (tokens[5], )

def interpreter(file_name):
    with open(file_name) as program:
        for cmd in program:
            # Push
            if cmd[0] == "PUSH":
                main.append(cmd[1])

            # Pop
            if cmd[0] == "POP":
                main.pop()

            # Add
            if cmd[0] == "ADD":
                x = main.pop()
                y = main.pop()
                main.append(x + y)

            # Subtract
            if cmd[0] == "SUB":
                x = main.pop()
                y = main.pop()
                main.append(y - x)

            # Print
            if cmd[0] == "PRINT":
                print(main.pop(), end="")

            # Println
            if cmd[0] == "PRINTLN":
                print(main.pop())

if __name__ == "__main__":
    if len(_argv) != 2:
        print("bad arg count")
        exit(1)
    interpreter(_argv[1])
