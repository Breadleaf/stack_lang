from sys import argv

def stk_push(stack, el):
    stack.append(el)

def stk_pop(stack):
    stack.pop()

def stk_dup(stack):
    stack.append(stack[0])

def stk_add(stack):
    stack.append(stack.pop() + stack.pop())

def stk_ston(stack):
    stack.append(float(stack.pop()))

def stk_eql(stack):
    tmp1 = stack.pop()
    tmp2 = stack.pop()
    tmp3 = (tmp1 == tmp2)
    stack.append(tmp2)
    stack.append(tmp1)
    stack.append(tmp3)

def stk_prt(stack):
    print(stack.pop(), end="")

def stk_prtln(stack):
    print(stack.pop())

def stk_prtstk(stack):
    print(stack)

def run_program(_list):
    main = []
    for command in _list:
        if command not in ["pop", "dup", "add", "eql", "prt", "prtln", "prtstk", "ston"]:
            stk_push(main, command)
        if command == "pop":
            stk_pop(main)
        if command == "dup":
            stk_dup(main)
        if command == "add":
            stk_add(main)
        if command == "ston":
            stk_ston(main)
        if command == "eql":
            stk_eql(main)
        if command == "prt":
            stk_prt(main)
        if command == "prtln":
            stk_prtln(main)
        if command == "prtstk":
            stk_prtstk(main)

def parse_program(file):
    return [line.strip() for line in file.split(" ")]

if __name__ == "__main__":
    if len(argv) != 2:
        print("Wrong arg count")
        exit(1)

    run_program(parse_program(open(argv[1]).read()))
