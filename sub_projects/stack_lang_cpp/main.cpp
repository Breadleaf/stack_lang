#include <iostream>
#include <variant>
#include <fstream>
#include <vector>
#include <string>
#include <map>

using namespace std;
using StkObject = variant<double, string>;
vector<StkObject> main_stack;
vector<StkObject> data_stack;
map<string, StkObject> variables;
map<string, StkObject> constants;

void stk_print_helper(StkObject obj)
{
	if (string *_str = get_if<string>(&obj)) cout << *_str << endl;
	if (double *_dbl = get_if<double>(&obj)) cout << *_dbl << endl;
}

void stk_print(vector<StkObject> &tmp_stk)
{
	stk_print_helper(tmp_stk.back());
	tmp_stk.pop_back();
}

void stk_add(vector<StkObject> &tmp_stk)
{
	std::string tmp_str;
	double tmp_num;
	if (double *_dbl = get_if<double>(tmp_stk.back()
}

void stk_sub(vector<StkObject> &tmp_stk)
{
}

void stk_mult(vector<StkObject> &tmp_stk)
{
}

void stk_div(vector<StkObject> &tmp_stk)
{
}

// Area for testing programs with hard coded values
void program()
{
	main_stack.push_back("Hi Ange, ");
	main_stack.push_back("I love you <3");
}

void errorGen(std::string errorMessage, int exitCode)
{
        std::cerr << errorMessage;
        exit(exitCode);
}

int main(int argc, char **argv)
{
        // Check and handle argument count
        if (argc == 1) { std::cout << "interpreter session\n"; return 0; }
        else if (argc == 2) { std::cout << "file session\n"; }
        else errorGen("Invalid argument count\n", 1);

        // File handeling
        std::ifstream file(argv[1]);
        if (file.fail()) errorGen("no file: '" + (std::string)argv[1] + "'\n", 2);
        return 0;
}
