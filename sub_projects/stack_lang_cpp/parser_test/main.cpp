#include <iostream>
#include <fstream>
#include <variant>
#include <vector>
#include <string>

using namespace std;
using StkObject = std::variant<double, std::string>;

class node
{
        public:
                node * left;
                node * right;
                std::variant<std::string, StkObject>;
};

void errorGen(std::string errorMessage, int exitCode)
{
        std::cerr << errorMessage;
        exit(exitCode);
}

int main(int argc, char **argv)
{
        // Check and handle argument count
        if (argc == 1) { std::cout << "interpreter session\n"; return 0; }
        else if (argc == 2) { std::cout << "file session\n"; }
        else { errorGen("Invalid argument count\n", 1); }

        // File handeling
        std::ifstream file(argv[1]);
        if (file.fail()) errorGen("no file: '" + (std::string)argv[1] + "'\n", 2);

        // Read the file parsed by the delimiter
        string line;
        while (!file.eof())
        {
                getline(file, line);
        }

        return 0; //////////////////////////////////////////////////////////////
}
