#!/bin/python3

from sys import argv

tokens = [
        # Types
        "NUM",  # Number
        "STR",  # String
        "CMT",  # Comment

        # Operators
        "ADD",  # Add
        "SUB",  # Subtract
        "MUL",  # Multiply
        "DIV",  # Divide

        # Functions
        "STON",   # String to number
        "NTOS",   # Number to string
        "PRINT",  # Print (without newline)
        "PRTLN",  # Print (with newline)

        # Assignments
        "LET",
        "CONST",
]

current_line = 0

class LineError(Exception):
    pass

# Interactively looks at the output of the lexer and tokenizes everything
def tokenizer():
    pass


# Returns a list of tuples containing token and values
def lexer(_line):
    return _line


def interpret_code(file_name):
    global current_line
    with open(file_name) as program:
        for line in program:
            current_line += 1
            line = lexer(line.strip())
            print(line)


def print_help(exit_code):
    print("Usage: stang [FILE PATH]")
    print("Stack Lang (stang for short) is a stack based interpreted programming language.")
    print("Stang was created with the intent of being a mathmatical computation language,")
    print("however it is being further developed into a general purpose programming language.")
    exit(exit_code)


if __name__ == "__main__":
    # Check for proper argument length
    if len(argv) > 2:
        print("Error: Too many arguments provided!\n")
        print_help(1)
    if len(argv) < 2:
        print("Error: Too few arguments provided!\n")
        print_help(1)
    if argv[1] == "-h" or argv[1] == "--help":
        print_help(0)

    # Try to run the code process any resulting errors
    try:
        interpret_code(argv[1])
    except FileNotFoundError:
        print(f"Error: No such file {argv[1]}!")
        exit(1)
    except LineError:
        print(f"Error found on line {current_line}")
    except Exception as ex:
        print(f"Unrecognized error: {type(ex)}")
        exit(1)
