#!/bin/python3

from platform import release as platform_release
from platform import system as platform_system
from os import system as os_system
from sys import argv

line_count = 1
version = "beta 1.0.0"

def interpreter(_list):
    global line_count
    if _list[0] == "!":
        os_system(f"{_list[1:]}")
    if _list == "exit":
        print("Exiting")
        exit(0)

def parser(_string):
    return _string

def line_by_line():
    global line_count
    print(f"Stack Lang | version : {version}")
    print(f"platform : {platform_system()} {platform_release()}\n")
    while True:
        interpreter(parser(input(f"[{line_count}]: ")))
        print()
        line_count += 1

def file_by_line(file_name):
    global line_count
    with open(file_name) as program:
        for line in program:
            interpreter(parser(line))
            line_count += 1


if __name__ == "__main__":
    if len(argv) == 1:
        line_by_line()
    elif len(argv) == 2:
        file_by_line(argv[1])
    else:
        print("Too many args!")
        exit(1)
