path_to_stable = ./versions/stable/
path_to_unstable = ./versions/unstable/

all:
	if test -f ~/.local/bin/stang; then make uninstall; else make stable_helper; fi

unstable:
	if test -f ~/.local/bin/stang; then make uninstall; else make unstable_helper; fi

update:
	git pull
	make uninstall
	@# I didnt know that select wasnt posix...
	echo "What version would you like to install" && echo "1) stable" && echo "2) unstable" && read -p "#? " opt && case $$opt in 1) echo Installing Stable...; make stable_helper;; 2) echo Installing Unstable...; make unstable_helper;; *) echo Installing Stable...; make stable_helper;; esac

stable_helper: stable_compile install

unstable_helper: unstable_compile install

stable_compile:
	nuitka3 --follow-imports --remove-output $(path_to_stable)stang.py -o stang

unstable_compile:
	nuitka3 --follow-imports --remove-output $(path_to_unstable)stang.py -o stang

install:
	mv stang ~/.local/bin/stang

uninstall:
	if test -f ~/.local/bin/stang; then rm ~/.local/bin/stang; fi

debug:
	ls $(path_to_stable) $(path_to_unstable)
