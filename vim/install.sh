#!/bin/bash

mkdir ~/.vim/syntax/
cp ./stk.vim ~/.vim/syntax/
mkdir ~/.vim/ftdetect/
echo "au BufRead,BufNewFile *.stk set filetype=stk" > ~/.vim/ftdetect/stk.vim
