" Vim syntax file
" Language: Stang (Stack Lang)
" Maintainer: Bradley Hutchings
" Latest Revision: June 28 2022

if exists("b:current_syntax")
	finish
endif

" Keywords
syn keyword mainKeywords push ++ pop --
syn keyword stackKeywords save swap print_stack empty swap_idx rev str_to_int stoi str_to_float stof
syn keyword mathKeywords add + sub - mult * div / pi e
syn keyword ioKeywords print println input
syn keyword stk_vars let const

" Integers
syn match number '\d\+'
syn match number '[-+]\d\+'
syn match number '[-+]\d\+\.\d*'


" Strings
syntax region strHi start=/\v"/ skip=/\v\\./ end=/\v"/

" Comments
syn keyword Todo contained TODO NOTE
syn match Comment "#.*$" contains=Todo

" Color time!
let b:current_syntax = "stang"

highlight link strHi      String
hi def link mainKeywords  Special
hi def link stackKeywords Special
hi def link mathKeywords  Error
hi def link ioKeywords    Type
hi def link Todo          Todo
hi def link Comments      Comment
hi def link number        Type
hi def link stk_vars      Type
