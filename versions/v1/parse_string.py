# This needs a lot of work but for now it is a lot better than the
# previous solution. Idealy it will hangle string escape codes and
# two strings improperly spaced ex: "Hello""World"
def parseLine(_line):
        final_list = []
        final_string = ""
        _in_string = False
        for idx, char in enumerate(_line):
            if char == "\"":
                _in_string = not(_in_string)
            if not _in_string and char != " ":
                final_string += char
            if _in_string:
                final_string += char
            if not _in_string and char == " ":
                final_list.append(final_string)
                final_string = ""
        final_list.append(final_string)
        return final_list
