from sessions import interactive_session
from sessions import file_session
from sys import argv

if __name__ == "__main__":
    if len(argv) == 1:
        interactive_session()
    if len(argv) == 2:
        file_session(argv[1])
    else:
        print("Error: Invalid arg count!")
        exit(1)

