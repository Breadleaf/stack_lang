from global_vars import stack
from global_vars import stack_clone
from global_vars import variables
from global_vars import constants

def convert_list_to_string(_list):
    return " ".join(_list)[1:-1]

def is_string(_list):
    tmp = " ".join(_list)
    if (tmp[0] == "\"" and tmp[-1] == "\""):
        return True
    return False

def divide_helper(one, two):
    if (two == 0):
        print("Divide by zero error!")
        exit(1)
    return one / two

def interpret(cmds_list):
    global stack
    global stack_clone
    global variables
    global constants
    tmp_stk = []
    garbage = ""
    keywords = {
        "insert"       : ["push", "++"],
        "remove"       : ["pop", "--"],
        "str_to_int"   : ["str_to_int", "stoi"],
        "str_to_float" : ["str_to_float", "stof"],
        "+"            : ["add", "+"],
        "-"            : ["sub", "-"],
        "*"            : ["mult", "*"],
        "/"            : ["div", "/"],
        "print"        : ["print"],
        "println"      : ["println"],
        "input"        : ["input"],
        "reverse"      : ["rev"],
        "prt_stk"      : ["print_stack"],
        "save_stack"   : ["save"],
        "swap_stack"   : ["swap"],
        "empty_stack"  : ["empty"],
        "swap_index"   : ["swap_idx"],
    }

    for words in cmds_list:
        if (words[0] in keywords["insert"]):
            stack.append(convert_list_to_string(words[1:]) if (is_string(words[1:])) else words[1])
            continue
        if (words[0] in keywords["remove"]):
            garbage = stack.pop()
            continue
        if (words[0] in keywords["str_to_int"]):
            stack.append(int(stack.pop()))
            continue
        if (words[0] in keywords["str_to_float"]):
            stack.append(float(stack.pop()))
            continue
        if (words[0] in keywords["+"]):
            a = stack.pop()
            b = stack.pop()
            stack.append(a + b)
            continue
        if (words[0] in keywords["-"]):
            a = stack.pop()
            b = stack.pop()
            stack.append(b - a)
            continue
        if (words[0] in keywords["*"]):
            a = stack.pop()
            b = stack.pop()
            stack.append(a * b)
            continue
        if (words[0] in keywords["/"]):
            a = stack.pop()
            b = stack.pop()
            stack.append(divide_helper(b, a))
            continue
        if (words[0] in keywords["print"]):
            print(stack.pop(), end="")
            continue
        if (words[0] in keywords["println"]):
            print(stack.pop())
            continue
        if (words[0] in keywords["input"]):
            stack.append(input(stack.pop()))
            continue
        if (words[0] in keywords["reverse"]):
            stack.reverse()
            continue
        if (words[0] in keywords["prt_stk"]):
            if (len(words) == 1  or words[1] == "1"):
                print(stack)
            elif (words[1] == "2"):
                print(stack_clone)
            else:
                print(f"Invalid stack: {words[1]}")
                exit(1)
            continue
        if (words[0] in keywords["save_stack"]):
            stack_clone = stack.copy()
        if (words[0] in keywords["swap_stack"]):
            tmp_stk = stack.copy()
            stack = stack_clone.copy()
            stack_clone = tmp_stk.copy()
            tmp_stk.clear()
            continue
        if (words[0] in keywords["empty_stack"]):
            stack.clear()
        if (words[0] in keywords["swap_index"]):
            idx_main = int(words[1])
            idx_copy = int(words[2])
            tmp = stack[idx_main]
            stack[idx_main] = stack_clone[idx_copy]
            stack_clone[idx_copy] = tmp
            continue
        if (words[0] == "let" and len(words) == 2 and words[1] not in [*constants]):
            variables.update({words[1] : stack.pop()})
            continue
        if (words[0] == "let" and words[1] not in [*constants]):
            tmp = convert_list_to_string(words[2:]) if (is_string(words[2:])) else words[2]
            variables.update({words[1] : tmp})
            continue
        if (words[0] == "const" and len(words) == 2 and words[1] not in [*variables]):
            constants.update({words[1] : stack.pop()})
            continue
        if (words[0] == "const" and words[1] not in [*variables]):
            tmp = convert_list_to_string(words[2:]) if (is_string(words[2:])) else words[2]
            constants.update({words[1] : tmp})
            continue
        if (words[0] in [*variables]):
            stack.append(variables[words[0]])
            del variables[words[0]]
            continue
        if (words[0] in [*constants]):
            stack.append(constants[words[0]])
            continue
        if (words[0] == "exit"):
            if len(words) == 1:
                exit(0)
            else:
                exit(words[1])

        invalid_used = True
        for value in keywords.values():
            if (words[0] in value):
                invalid_used = False
        if (invalid_used):
            print(f"Error: Nonexistent keyword used! '{words[0]}'")
            exit(1)
