def _parseHelper(_line):
        final_list = []
        final_string = ""
        _in_string = False
        for idx, char in enumerate(_line):
            if char == "\"":
                _in_string = not(_in_string)
            if not _in_string and char != " ":
                final_string += char
            if _in_string:
                final_string += char
            if not _in_string and char == " ":
                final_list.append(final_string)
                final_string = ""
        final_list.append(final_string)
        return final_list

# TODO: Fix names, they do not tell anything that is happening
def parseLine(_line):
    program = _line.split("\n")
    tmp = ""
    for line in program:
        if line == "":
            continue
        tmp += line

    tmp = tmp.split(";")[:-1]

    final = []
    for tmp2 in tmp:
        x = _parseHelper(tmp2)
        if x[0] == "#":
            continue
        final.append(x)

    operations = []
    for tmp in final:
        tmp_op = []
        for tmp2 in tmp:
            if tmp2 == "\n":
                pass
            try:
                tmp_op.append(str(eval(eval(tmp2))))
            except Exception as ex:
                tmp_op.append(tmp2)
        operations.append(tmp_op)

    return operations

def parseFile(file_name):
    return parseLine(open(file_name).read())
