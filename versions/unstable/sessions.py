from platform import release as platform_release
from platform import system as platform_system
from os import system as os_system
from interpreter import interpret
from parseFuncs import parseLine
from parseFuncs import parseFile

def interactive_session(version="alpha 1.0.1"):
    line_count = 1
    print(f"Stack Lang | version : {version}")
    print(f"platform : {platform_system()} {platform_release()}\n")
    while True:
        _input = input(f"[{line_count}]: ")
        if _input[0] == "!":
            os_system(_input[1:])
            print()
            line_count += 1
            continue
        interpret(parseLine(_input))
        print()
        line_count += 1

def file_session(file_name):
    interpret(parseFile(file_name))
