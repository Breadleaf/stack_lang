# Stack Lang
Stack Lang (stang for short) is a stack based interpreted programming language.
Stang was created with the intent of being a mathmatical computation language, however it is being further developed into a general purpose programming language.

### How to install stang (latest stable release):

```
git clone https://gitlab.com/breadleaf/stack_lang
cd stack_lang
make
```

### How to install stang (unstable):

```
git clone https://gitlab.com/breadleaf/stack_lang
cd stack_lang
make unstable
```

### How to uninstall stang (automatic):

```
cd stack_lang
make
```

### How to update to the newest version of stang:

Please note that newer stable releases may no longer work with older .stk files.
Please check the changelog for any syntax changes.

```
cd stack_lang
make update
```
